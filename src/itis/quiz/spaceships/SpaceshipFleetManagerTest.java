package itis.quiz.spaceships;




import java.util.ArrayList;


public class SpaceshipFleetManagerTest {

    CommandCenter commandCenter = new CommandCenter();
    static float points = 0;

    public static void main(String[] args) {

        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest();

        boolean test1_1 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShipExists_returnFirst();
        boolean test1_2 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShipExists_returnNull();
        boolean test1_3 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerFulShip_returnFirst();
        boolean test2_1 = spaceshipFleetManagerTest.getShipByName_shipFound();
        boolean test2_2 = spaceshipFleetManagerTest.getShipByName_shipNotFound();
        boolean test3_1 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_returnList();
        boolean test3_2 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_returnVoid();
        boolean test4_1 = spaceshipFleetManagerTest.getAllCivilianShips_returnList();
        boolean test4_2 = spaceshipFleetManagerTest.getAllCivilianShips_returnVoid();

        if (test1_1) {
            System.out.println("Тест 1.1 выполнен ");
            points += 0.5;
        }

        if (test1_2) {
            System.out.println("Тест 1.2 выполнен");
            points += 0.5;
        }

        if (test1_3) {
            System.out.println("Тест 1.3 выполнен");
            points += 0.5;
        }
        if (test2_1) {
            System.out.println("Тест 2.1 вополнен");
            points += 0.5;
        }
        if (test2_2) {
            System.out.println("Тест 2.2 выполнен");
            points += 0.5;
        }
        if (test3_1) {
            System.out.println("Тест 3.1 выполнен");
            points += 0.5;
        }
        if (test3_2) {
            System.out.println("Тест 3.2 выполнен");
            points += 0.5;
        }
        if (test4_1) {
            System.out.println("Тест 4.1 выполнен");
            points += 0.5;
        }
        if (test4_2) {
            System.out.println("Тест 4.2 выполнен");
            points += 0.5;
        }

        System.out.println("Выполнилось " + points + " из 4.5");

    }

    //Возвращает самый хорошо вооруженный корабль (с самой большой огневой мощью, отличной от нуля).
    // Если таких кораблей несколько, возвращает первый по списку.
    // Если подходящего корабля нет, возвращает null.
    private boolean getMostPowerfulShip_mostPowerfulShipExists_returnFirst() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 200, 0, 0));
        testShipsList.add(new Spaceship("Doo", 100, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);

        if (result.getFirePower() == 200 && result.getName().equals("Foo")) {
            return true;
        }
        return false;
    }
    private boolean getMostPowerfulShip_mostPowerfulShipExists_returnNull() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 0, 0, 0));
        testShipsList.add(new Spaceship("Doo", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);

        return result == null;
    }

    private boolean getMostPowerfulShip_mostPowerFulShip_returnFirst() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 300, 0, 0));
        testShipsList.add(new Spaceship("Doo", 300, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        if (result.getName().equals("Foo") && result.getFirePower() == 300) {
            return true;
        } else {
            return false;
        }
    }

    //Возвращает корабль с заданным именем (предполагается что имена кораблей уникальны).
    // Если подходящего корабля нет, возвращает null.
    private boolean getShipByName_shipFound() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 0, 0));
        testShipsList.add(new Spaceship("Doo", 20, 0, 0));

        Spaceship result = commandCenter.getShipByName(testShipsList, "Foo");

        if (result.getName().equals("Foo")) {
            return true;
        }
        return false;
    }

    private boolean getShipByName_shipNotFound() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 10, 10, 10));
        testShipsList.add(new Spaceship("Doo", 10, 10, 10));

        Spaceship result = commandCenter.getShipByName(testShipsList, "кораблик");

        if (result == null) {
            return true;
        }
        return false;
    }

    //Возвращает только корабли с достаточно большим грузовым трюмом для перевозки груза заданного размера.
    //Если подходящих кораблей нет, возвращает пустой список.
    private boolean getAllShipsWithEnoughCargoSpace_returnList() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 300, 0));
        testShipsList.add(new Spaceship("Doo", 100, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 300);

        if (result.get(0).getCargoSpace() >= 300) {
            return true;
        }
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_returnVoid() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 300, 0));
        testShipsList.add(new Spaceship("Doo", 100, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 10000);

        if (result.isEmpty()) {
            return true;
        }
        return false;
    }


    //Возвращает только "мирные" корабли (не оснащенные вооружением, без огневой мощи).
    //Если подходящих кораблей нет, возвращает пустой список.
    private boolean getAllCivilianShips_returnList() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 0, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);

        if (result.get(0).getFirePower() == 0) {
            return true;
        }
        return false;
    }

    private boolean getAllCivilianShips_returnVoid() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 100, 400, 0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);

        if (result.isEmpty()) {
            return true;
        }
        return false;
    }
}
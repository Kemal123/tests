package itis.quiz.spaceships;
import org.junit.jupiter.api.*;
import java.util.ArrayList;

public class Test {

    CommandCenter commandCenter = new CommandCenter();

    static ArrayList<Spaceship> testList;

    @BeforeAll
    static void createList() {
        testList = new ArrayList<>();
    }


    @AfterEach
    void cleaningArrayList() {
        testList.clear();
    }


    @DisplayName("Возвращает самый хорошо вооруженный корабль")
    @org.junit.jupiter.api.Test
    void test1_1() {
        testList.add(new Spaceship("Foo", 500, 0, 0));
        testList.add(new Spaceship("Doo", 50, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        Assertions.assertEquals(result.getFirePower(), 500);
    }

    @DisplayName("Должен вывести null, если подаются корабли с нулевым уроном")
    @org.junit.jupiter.api.Test
    void test1_2() {
        testList.add(new Spaceship("Foo", 0, 0, 0));
        testList.add(new Spaceship("Doo", 0, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        Assertions.assertNull(result);
    }

    @DisplayName("Выводит первый корабль, если у кораблей одинаковый уроном")
    @org.junit.jupiter.api.Test
    void test1_3() {
        testList.add(new Spaceship("Foo", 1000, 0, 0));
        testList.add(new Spaceship("Doo", 1000, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        Assertions.assertEquals(result.getName(), "Foo");
    }

    @DisplayName("Выводит корабль с заданым именем")
    @org.junit.jupiter.api.Test
    void test2_1() {
        testList.add(new Spaceship("Foo", 1000, 0, 0));
        testList.add(new Spaceship("Doo", 1000, 0, 0));
        Spaceship result = commandCenter.getShipByName(testList, "Foo");
        Assertions.assertEquals(result.getName(), "Foo");
    }

    @DisplayName("Выводит null, если у кораблей нет урона ")
    @org.junit.jupiter.api.Test
    void test2_2() {
        testList.add(new Spaceship("Foo", 0, 0, 0));
        testList.add(new Spaceship("Doo", 0, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        Assertions.assertNull(result);
    }

    @DisplayName("Возвращает только корабли с достаточно большим грузовым трюмом для перевозки груза заданного размера.")
    @org.junit.jupiter.api.Test
    void test3_1() {
        testList.add(new Spaceship("Foo", 0, 502, 0));
        testList.add(new Spaceship("Doo", 0, 200, 0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 500);
        Assertions.assertEquals(result.get(0).getName(), "Foo");
    }

    @DisplayName("Возвращает null, если нет кораблей с достаточно большим трюмом")
    @org.junit.jupiter.api.Test
    void test3_2() {
        testList.add(new Spaceship("Foo", 0, 502, 0));
        testList.add(new Spaceship("Doo", 0, 200, 0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 50000);
        Assertions.assertTrue(result.isEmpty());
    }

    @DisplayName("Возвращает только мирные корабли")
    @org.junit.jupiter.api.Test
    void test4_1() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 0, 400, 0));
        testShipsList.add(new Spaceship("Doo", 100, 200, 0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        Assertions.assertEquals(result.get(0).getName(), "Foo");
    }

    @DisplayName("Если мирных кораблей нет, то выводит null")
    @org.junit.jupiter.api.Test
    void test4_2() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 100, 200, 0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        Assertions.assertTrue(result.isEmpty());

    }
}

